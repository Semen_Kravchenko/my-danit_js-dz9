let tab = function() {
    let tabs = document.querySelectorAll('.tabs-title');
    let tabCont = document.querySelectorAll('.tabs-content-item');
    let tabName;
    tabs.forEach(i => {
        i.addEventListener('click', selTabs);
    });

    function selTabs() {
        tabs.forEach(i => {
            i.classList.remove('active');
        });
        this.classList.add('active');
        tabName = this.getAttribute('data-tab-name');
        selTabCont(tabName);
    }

    function selTabCont(tabName) {
        tabCont.forEach(i => {
            i.classList.contains(tabName) ? i.classList.add('active') : i.classList.remove('active');
        });
    }

};

tab();
